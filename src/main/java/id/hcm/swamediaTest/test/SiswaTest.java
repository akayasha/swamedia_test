package id.hcm.swamediaTest.test;

import id.hcm.swamediaTest.config.repository.SiswaRepository;
import id.hcm.swamediaTest.model.MataPelajaran;
import id.hcm.swamediaTest.model.Siswa;
import id.hcm.swamediaTest.service.SiswaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SiswaTest {

    @InjectMocks
    private SiswaService siswaService;

    @Mock
    private SiswaRepository siswaRepository;

    private Siswa testSiswa;

    private String testSiswaId = String.valueOf(1L);

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this); // Initialize the annotated mocks
        testSiswa = new Siswa();
        testSiswa.setNamaSiswa("kiko");
        testSiswa.setNis(testSiswaId);
        testSiswa.setKelas("7A");
        testSiswa.setJenisKelamin("Laki");
    }

//    @Test
//    public void testSaveSiswa() {
//        // Mock the behavior of the repository save method
//        when(siswaRepository.save(Mockito.any())).thenReturn(testSiswa);
//
//        Siswa savedMataPelajaran = SiswaService.save(testSiswa);
//        assertEquals(testSiswa, savedMataPelajaran);
//    }

}
