package id.hcm.swamediaTest.test;

import id.hcm.swamediaTest.config.repository.MataPelajaranRepository;
import id.hcm.swamediaTest.model.MataPelajaran;
import id.hcm.swamediaTest.service.MataPelajaranService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MataPelajaranTest {

    @InjectMocks
    private MataPelajaranService mataPelajaranService;

    @Mock
    private MataPelajaranRepository mataPelajaranRepository;

    private MataPelajaran tesMataPelajaran;

    private String tesMataPelajaranId = String.valueOf(1L);

    @BeforeEach
    public void setUp() {
        tesMataPelajaran = new MataPelajaran();
        tesMataPelajaran.setCode(tesMataPelajaranId);
        tesMataPelajaran.setNamaPelajaran("Test Mata Nama Pelajaran");
    }

    @Test
    public void testCreateMataPelajaran() {
        when(mataPelajaranRepository.save(Mockito.any())).thenReturn(tesMataPelajaran);

        MataPelajaran savedMataPelajaran = mataPelajaranService.save(tesMataPelajaran);

        assertEquals(tesMataPelajaran, savedMataPelajaran);
    }

    @Test
    public void  testGetNamaMataPelajaran(){
        List<MataPelajaran> mataPelajaranList = new ArrayList<>();
        mataPelajaranList.add(tesMataPelajaran);

        when(mataPelajaranRepository.findAll()).thenReturn(mataPelajaranList);

        List<MataPelajaran> retrievedMataPelajaranList = mataPelajaranService.findAll();

        assertEquals(mataPelajaranList.size(),retrievedMataPelajaranList.size());
        assertEquals(mataPelajaranList,retrievedMataPelajaranList);

    }

    @Test
    public void testGetMapelByCode(){
        String codeToFind = tesMataPelajaranId;

        when(mataPelajaranRepository.findById(codeToFind)).thenReturn(Optional.of(tesMataPelajaran));

        Optional<MataPelajaran> foundMataPelajaran = mataPelajaranService.findByCode(codeToFind);

        assertTrue(foundMataPelajaran.isPresent());
        assertEquals(tesMataPelajaran,foundMataPelajaran.get());

    }

    @Test
    public void testGetNamaMapel() {
        String namaPelajaranToFind = "Test Mata Nama Pelajaran";
        List<MataPelajaran> mataPelajaranList = new ArrayList<>();
        mataPelajaranList.add(tesMataPelajaran);

        when(mataPelajaranRepository.findByNamaPelajaranContaining(namaPelajaranToFind)).thenReturn(mataPelajaranList);

        List<MataPelajaran> retrievedMataPelajaranList = mataPelajaranService.findByNamaPelajaran(namaPelajaranToFind);

        assertEquals(mataPelajaranList.size(), retrievedMataPelajaranList.size());
        assertEquals(mataPelajaranList, retrievedMataPelajaranList);
    }

    @Test
    public void testDeleteMapel(){
        mataPelajaranService.delete(tesMataPelajaranId);
    }


}
