package id.hcm.swamediaTest.test;

import id.hcm.swamediaTest.config.repository.MataPelajaranRepository;
import id.hcm.swamediaTest.config.repository.NilaiRepository;
import id.hcm.swamediaTest.config.repository.SiswaRepository;
import id.hcm.swamediaTest.model.MataPelajaran;
import id.hcm.swamediaTest.model.Nilai;
import id.hcm.swamediaTest.model.Siswa;
import id.hcm.swamediaTest.service.NilaiService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class NilaiTest {

    @InjectMocks
    private NilaiService nilaiService;

    @Mock
    private NilaiRepository nilaiRepository;

    @Mock
    private SiswaRepository siswaRepository;

    @Mock
    private MataPelajaranRepository mataPelajaranRepository;

    private MataPelajaran testMataPelajaran;

    private Siswa testSiswa;

    private Nilai testNilai;

    private Long tesNilaiId = 1L;

    @BeforeEach
    public void setUp(){
        testNilai = new Nilai();
        testNilai.setId(tesNilaiId);
        testNilai.setSemester(1);
        testNilai.setNis("AB10");
        testNilai.setSemester(1);
        testNilai.setKodeNilai("A");
        testNilai.setNilai(95.0f);

        testMataPelajaran = new MataPelajaran();
        testMataPelajaran.setCode("ABC");
        testMataPelajaran.setNamaPelajaran("Math");

        testSiswa = new Siswa();
        testSiswa.setNis("AB10");
        testSiswa.setNamaSiswa("KUKU");
    }

    @Test
    public void testSaveNilai(){
        testNilai.setCodeMataPelajaran("ABC");

        Mockito.lenient().when(mataPelajaranRepository.findByCode(Mockito.anyString())).thenReturn(testMataPelajaran);
        Mockito.lenient().when(siswaRepository.findByNis(Mockito.anyString())).thenReturn(testSiswa);
        Mockito.lenient().when(nilaiRepository.save(Mockito.any())).thenReturn(testNilai);

        Nilai savedNilai = nilaiService.save(testNilai);
        assertEquals(testNilai, savedNilai);
    }

    @Test
    public void testFindAllNilai() {
        List<Nilai> nilaiList = new ArrayList<>();
        nilaiList.add(testNilai);

        when(nilaiRepository.findAll()).thenReturn(nilaiList);

        List<Nilai> retrievedNilaiList = nilaiService.findAll();

        assertEquals(nilaiList.size(),retrievedNilaiList.size());
        assertEquals(nilaiList, retrievedNilaiList);
    }

    @Test
    public void testFindNilaiById(){
        when(nilaiRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(testNilai));

        Optional<Nilai> foundNilai = nilaiService.findById(tesNilaiId);

        assertTrue(foundNilai.isPresent());
        assertEquals(testNilai,foundNilai.get());
    }

    @Test
    public void testDeleteNilai(){
        nilaiService.delete(tesNilaiId);
    }

    @Test
    public void testFindByNisAndSemesterAndKodeNilai(){
        List<Nilai> nilaiList = new ArrayList<>();
        nilaiList.add(testNilai);

        when(nilaiRepository.findByNisAndSemesterAndKodeNilai(Mockito.anyString(), Mockito.anyInt(), Mockito.anyString())).thenReturn(nilaiList);

        List<Nilai> foundNilaiList = nilaiService.findByNisAndSemesterAndKodeNilai("SIS001", 1, "A");
        assertEquals(nilaiList.size(),foundNilaiList.size());
        assertEquals(nilaiList,foundNilaiList);
    }

}
