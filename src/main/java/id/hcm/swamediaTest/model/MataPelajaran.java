package id.hcm.swamediaTest.model;

import javax.persistence.*;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@ApiModel(description = "Siswa model information")
@Entity
@Table(name = "mata_pelajaran")
public class MataPelajaran implements Serializable {

    @ApiModelProperty(value = "Mapel id")
    @Id
    @Column(name = "code", unique = true)
    private String code;

    @ApiModelProperty(value = "Mapel Nama")
    @Column(name = "namaPelajaran")
    private String namaPelajaran;


    // Constructors, getters, and setters

    public MataPelajaran() {
    }

    public MataPelajaran(String code, String namaPelajaran) {
        this.code = code;
        this.namaPelajaran = namaPelajaran;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNamaPelajaran() {
        return namaPelajaran;
    }

    public void setNamaPelajaran(String namaPelajaran) {
        this.namaPelajaran = namaPelajaran;
    }
}
