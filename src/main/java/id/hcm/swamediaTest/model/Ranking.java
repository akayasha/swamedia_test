package id.hcm.swamediaTest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@ApiModel(description = "Ranking model information")
@Entity
@Table(name = "ranking")
public class Ranking {

    @ApiModelProperty(value = "Ranking id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty(value = "Ranking Nis")
    @Column(name = "nis")
    private String nis;

    @ApiModelProperty(value = "Ranking Semester")
    @Column(name = "semester")
    private Integer semester;

    @ApiModelProperty(value = "Ranking ranking")
    @Column(name = "ranking")
    private Integer ranking;

    @ApiModelProperty(value = "Ranking Total Score")
    @Transient // This annotation marks the field as non-persistent
    private Float totalScore;

    @ApiModelProperty(value = "Ranking ranking")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nis", referencedColumnName = "nis", insertable = false, updatable = false)
    private Siswa siswa;

    // Constructors with parameters
    public Ranking(String nis, Integer semester,Siswa siswa) {
        this.nis = nis;
        this.semester = semester;
        this.siswa = siswa;
    }

    public Siswa getSiswa() {
        return siswa;
    }

    public void setSiswa(Siswa siswa) {
        this.siswa = siswa;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNis() {
        return nis;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }
}
