package id.hcm.swamediaTest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@ApiModel(description = "Siswa model information")
@Entity
@Table(name = "siswa")
public class Siswa {
    @ApiModelProperty(value = "Siswa id")
    @Id
    @Column(name = "nis") // Declare "nis" as the primary key
    private String nis;

    @ApiModelProperty(value = "Siswa name")
    @Column(name = "namaSiswa")
    private String namaSiswa;

    @ApiModelProperty(value = "Siswa Kelas")
    @Column(name = "kelas")
    private String kelas;

    @ApiModelProperty(value = "Siswa Jk")
    @Column(name =  "jenisKelamin")
    private String jenisKelamin;

    @ApiModelProperty(value = "Siswa List")
    @OneToMany(mappedBy = "siswa")
    private List<Nilai> nilaiList;

}
